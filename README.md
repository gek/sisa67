
# SISA67 - A simple RISC processor

Implemented in the SEABASS metaprogramming language.

In development

## FEATURES

* Userland with memory protection

* 256 64 bit general purpose registers

* can access 4 gigabytes (configurable) of memory

* Performance benchmarks on privileged code as fast as ~660.7 Million Instructions Per Second on an AMD Ryzen 3600

* Public domain- no IP attachments, take whatever you want




