
default:
	cbas isa.cbas isa.c
	cc -O3 -march=native -s isa.c dev.c -o s67 -lm -fwrapv -Wno-strict-aliasing -Wno-unused

clean:
	rm -f *.bin *.out *.exe isa.c auto_out.c isa s67
	
install: default
	admin cp ./s67 /usr/local/bin/
	
uninstall:
	admin rm -rf /usr/local/bin/s67

kill_shm:
	rm -rf /dev/shm/SISA67
	
rxincrmark:
	s67 -c test/rxincrmark.s67
	s67 out.bin
	
	
push: clean
	git add .
	git commit -m "make push"
	git push -f
	
