/*
    Device implementation...
    //device read
    fn public dread(u64 addr)->u64;
    //device write
    fn public dwrite(u64 addr, u64 v);
    
    This device implements the following features:
    
    1. getchar/putchar unlocked (nonblocking) on dev address 0
    2. A fixed-size disk
    3. A shared memory buffer which can be used to connect "peripherals"
        (i.e. other processes) to the system. 16 mutexes are provided
        at the beginning of shared memory to provide synchronization.
        
        
    This implementation was designed for the following purposes:
    
    1. It must be possible to implement a basic REPL quickly without any graphics.
        Basically, a UART. Waiting until I have a graphical dummy terminal implemented to be
        able to interact with my system at all would make it difficult to develop
        on the architecture.
    2. We want the fastest possible I/O for said REPL.
    3. We need to be able to expand this system in the future to include
        other peripherals beyond the initial debugging/development interface...
        such as a virtual GPU, graphical dummy terminals, Networking cards, etcetera.
        
    getchar and putchar provide 1. Non-blocking I/O can be done by fiddling with
    termios, fulfilling 2.
        
    Shared memory with mutexes provides a complete answer to 3. 
*/

#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <errno.h>
#include <pthread.h>


static struct termios oldChars;
static struct termios newChars;

FILE* disk = 0;
//disk size in bytes.
//we choose 512M
#define DISK_SIZE (1024 * 1024 * 512)
#define DISK_MASK (DISK_SIZE-1)

unsigned long long shouldquit = 0;
int have_exited = 0;

unsigned char* shared_memory_pointer = 0;
unsigned char* disk_map_ptr = 0;
int shared_memory_fd = 0;
char* shared_memory_name = "/SISA67";
char* disk_name = "sisa67.dsk";
pthread_mutex_t* mutexmem = 0;


#define SHARED_MEMORY_SIZE (256 * 1024 * 1024)
#define SHARED_MEMORY_MASK (SHARED_MEMORY_SIZE - 1)


#define SHARED_MUTEXES 0x10
#define SHARED_MUTEXES_MASK (SHARED_MUTEXES - 1)
void emu_respond(int bruh){
	(void)bruh;
	shouldquit = 0xffFFffFFffFFffFF;
	return;
}


static void initTermios(int echo)
{
    tcgetattr(STDIN_FILENO, &oldChars); /* grab old terminal i/o settings */
    newChars = oldChars; /* make new settings same as old settings */
    newChars.c_lflag &= ~ICANON; /* disable buffered i/o */
    newChars.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &newChars); /* use these new terminal i/o settings now */
}


static void dieTermios(){
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &oldChars); /* use these new terminal i/o settings now */	
}

void __CBAS__dcl(){
    if(have_exited) return;
    dieTermios();
    if(disk){
        fclose(disk);
        disk = 0;
    }
    if(shared_memory_fd && shared_memory_pointer != NULL){
        for(int i = 0; i < SHARED_MUTEXES; i++){
            pthread_mutex_destroy(mutexmem + i);
        }
        munmap(shared_memory_pointer, SHARED_MEMORY_SIZE);
        close(shared_memory_fd);
        shm_unlink(shared_memory_name);
        shared_memory_pointer = 0;
        shared_memory_fd = 0;
    }
    have_exited = 1;
    puts("<System Exited>");
}

void init_disk(){
    disk = fopen(disk_name, "wb");
    if(disk == 0){
        puts("<EMU ERROR>Could not initialize disk...");
        exit(1);
    }
    char* buf = calloc(1,DISK_SIZE);
    fwrite(buf, 1, DISK_SIZE, disk);
    fclose(disk);
    free(buf);
    disk = NULL;
}

void __CBAS__di(){
	initTermios(0);
	atexit(__CBAS__dcl);
	signal(SIGINT, emu_respond);
	
	disk = fopen(disk_name, "r+b");
	if(disk == NULL){
	    init_disk();
	    disk = fopen(disk_name, "r+b");
	}
	if(disk == NULL){
        puts("<EMU ERROR>Could not open disk...");
        exit(1);
	}
	//initialize shared memory...
	errno = 0;
	shared_memory_fd = shm_open(shared_memory_name, O_CREAT|O_RDWR|O_EXCL, 0755);
    if(shared_memory_fd == -1){
        perror("Could not open shared memory");
        exit(1);
    }
    ftruncate(shared_memory_fd, SHARED_MEMORY_SIZE);
	if(errno){
	    perror("ftruncate");
	    exit(1);
	}
	shared_memory_pointer = mmap(
	    0, 
	    SHARED_MEMORY_SIZE,  //length
	    PROT_READ | PROT_WRITE, //prot
	    MAP_SHARED, //flags
	    shared_memory_fd,
	    0
    );
    if(errno){
        perror("mmap");
        exit(1);
    }
    mutexmem = (void*)shared_memory_pointer;
    /*Initialize mutexes...*/
    for(int i = 0; i < SHARED_MUTEXES; i++){
        pthread_mutexattr_t a;
        pthread_mutexattr_init(&a);
        pthread_mutexattr_setpshared(&a, PTHREAD_PROCESS_SHARED);
        if(pthread_mutex_init(mutexmem + i, &a)){
            perror("pthread_mutex_init");
            exit(1);
        }
    }
}

unsigned long long __CBAS__dread(unsigned long long addr){
    if(addr == 0){
        return getchar_unlocked();
    }
    if(addr == 1){
        return clock();
    }
    if(addr == 2){
        return CLOCKS_PER_SEC;
    }
    if(addr == 3){
        return shouldquit;
    }
    if(addr == 4){
        return DISK_SIZE;
    }
    if(addr == 5){
        return SHARED_MEMORY_SIZE;
    }
    //how many mutexes are there?
    if(addr == 6){
        return SHARED_MUTEXES;
        //return 0;
    }
    //how much memory is taken up by a mutex?
    if(addr == 7){
        return sizeof(pthread_mutex_t);
        //return 0;
    }
    //access disk
    if((addr >> 48) == 0xD){
        addr = addr & DISK_MASK & ~7ul;
        fseek(disk, addr, SEEK_SET);
        fread(&addr, 1, 8,disk);
        return addr;
    }
    //access shared memory
    if((addr >> 48) == 0x3){
        addr = addr & SHARED_MEMORY_MASK & ~7ul;
        addr = ((unsigned long long*)(shared_memory_pointer + addr))[0];
        return addr;
    }
    //try locking...
    if((addr >> 48) == 0x4){
        addr = addr & SHARED_MUTEXES_MASK;
        int a = pthread_mutex_trylock(mutexmem + addr);
        return a == 0;
    }
    //unlock if locked...
    if((addr >> 48) == 0x5){
        addr = addr & SHARED_MUTEXES_MASK;
        int a = pthread_mutex_unlock(mutexmem + addr);
        return a == 0;
    }

    return 0;
}

void __CBAS__dwrite(unsigned long long addr, unsigned long long v){
    //putchar
    if(addr == 0){
        putchar_unlocked(v);
        return;
    }
    //magic
    if(addr == 0xDEADBEEF){
        exit(v);
    }
    if((addr >> 48) == 0xD){
        addr = addr & DISK_MASK & ~7ul;
        fseek(disk, addr, SEEK_SET);
        fwrite(&v, 1, 8,disk);
        return;
    }
    //reading and writing from shared memory....
    if((addr >> 48) == 0x3){
        addr = addr & SHARED_MEMORY_MASK & ~7ul;
        ((unsigned long long*)(shared_memory_pointer + addr))[0] = v;
        return;
    }
}


